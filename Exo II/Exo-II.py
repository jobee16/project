'''La Fonction lista prend en paramettre une liste qui calcule la probabilite de chaque element et qui les affiche aleatoirement'''

import os
import random

""" Random Numbers """
""" Exo """


#fonction renvoyant l'histogramme avec la probabilite de chaque element
def choose_from_hist(liste):    
        dic={}
        lst=[]
        #On Parcour la liste et faire le decompte des items
        for item in liste :
            if item not in dic.keys() :
                dic[item]=1
            else :
                    dic[item]=dic[item]+1
                    
        #La probabilite de l'element            
        for cle in dic :
            dic[cle]=str(dic[cle])+"/"+str(len(liste))
        lst=dic.keys()
        nbr=random.randint(0,len(lst))
        return lst[nbr],dic[lst[nbr]]
        return dic 

#Main test
if __name__ == "__main__":
    liste1=['a','a','b','a','f','s','s','a','f','a','s','d','d','s','e','e']
    Mot,proba=choose_from_hist(liste1)
    print "La probabilite de",Mot,"est de",proba
   
