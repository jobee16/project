
def printCommon(hist, number = 10):

    com_words = most_common(hist) # a list to hold the most common words
    
    print("The most " + str(number)+ " common words are")

    for freq, word in com_words[: number]:
        print(word + "\t" + str(freq))

def most_common(hist):

    com_words = [] # a list to hold the most common words

    for value, key in hist.items():
        com_words.append((key, value))
        com_words.sort(reverse = True)

    return lst_com_words
